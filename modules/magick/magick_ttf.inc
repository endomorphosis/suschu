<?php

function _magick_ttf_openfile($filename) {
  if (!is_file($filename)) {
    return FALSE;
  }
  $f = @fopen($filename, "rb");
  return $f;
}

function _magick_ttf_closefile($file) {
  return @fclose($file);
}

function _magick_ttf_header($file) {
  $contents = @fread($file, 12);
  if ($contents === false)
    return false;
  
  // Stored in big endian order
  $c = unpack("N1magic/n1tables/n1range/n1entry/n1shift", $contents);
  
  // Make sure there is 
  if (count($c) < 5)
    return false;

  // TTF requires that it be 0x000100000
  if ($c['magic'] != 0x10000)
    return false;
  
  return $c;
}

function _magick_ttf_parse_tables($file, $tables) {
  if ((!is_numeric($tables)) && ($tables > 1))
    return false;
    
  $contents = @fread($file, $tables * 16);
  if ($contents === false)
    return false;
    
  $entries = array();
  $table_entries = chunk_split($contents, 16, '');
  // process the table accordingly
  foreach ($table_entries as $table_entry) {
    // convert entry into array
    $entry = @unpack("A4tag/N1checksum/N1offset/N1length", $table_entry);
    // make sure it's the correct array
    if (count($entry) != 4)
      return false;
    // no good if no tag
    if (!isset($entry['tag']))
      return false;
    // store entry under tag name
    $entries[$entry['tag']] = $entry;
  }
  
  // no good if duplicate tags
  if (count($entries) != $tables)
    return false;
    
  return $entries;
}

function _magick_ttf_process_names($file, $name) {
  // This processes a name table entry
  if (!isset($name['offset']))
    return false;

  // Go to appropriate name table directory
  $loc = @fseek($file, $name['offset'] );
  $contents = @fread($file, 6);
  if ($contents === false)
    return false;

  // Get details for the following array
  $c = @unpack("n1format/n1records/n1offset", $contents);
  if (count($c) != 3)
    return false;
  // do error checking on the records
  if ($c['records'] < 1)
    return false;
  $c['start'] = $name['offset'];

  // otherwise read the entire table names
  $contents = @fread($file, $c['records'] * 12);
  if ($contents === false)
    return false;

  $table_entries = str_split($contents, 12);
  // process the table accordingly
  foreach ($table_entries as $table_entry) {
    // convert entry into array
    $entry = @unpack("n1platform/n1encoding/n1language/n1name/n1length/n1offset", $table_entry);
    // make sure it's the correct array
    if (count($entry) != 6)
      return false;
    // store entry under platform ID->name ID 
    $entries[$entry['platform']][$entry['name']] = $entry;
  }

  // ignore if there where duplicate tags, we're only processing
  // through what we need

  return array('names' => $c, 'details' => $entries );
}

function _magick_ttf_retrieve_string($file, $offset, $length, $unicode=false) {
  // This processes a name table entry
  if (!is_numeric($offset) || !is_numeric($length))
    return false;
    
  // Go to appropriate name table directory
  $loc = @fseek($file, $offset );
  $contents = @fread($file, $length );
  if ($contents === false)
    return false;

  if ($unicode) {
    // string the first part of the string
    $uni = @unpack("n*", $contents);
    $s = '';
    foreach ($uni as $c) {
      $s .= chr($c);
    }
  } else {
    $s = $contents;
  }
      
  return $s;
}

function _magick_ttf_platform($key) {
  switch ($key) {
    case 0:
      return "APPLE (UNICODE)";
    case 1:
      return "MACINTOSH";
    case 2:
      return "ISO (DEPRECATED)";
    case 3:
      return "MICROSOFT";
    case 4:
      return "CUSTOM";
    case 5:
      return "ADOBE";
    default:
      return "UNKNOWN";
  }
}

/*
  Processing of a TTF file
*/

function _magick_ttf_locate_name($filename) {
  // open filename
  $f = _magick_ttf_openfile($filename);
  if ($f === false) return false;

  $header = _magick_ttf_header($f);
  if ($header === false) return false;

  $tables = _magick_ttf_parse_tables($f, $header['tables']);
  if ($tables === false) return false;
  
  $names = _magick_ttf_process_names($f, $tables['name']);
  if ($names === false) return false;

  $unicode = false;
  $offset = $names['names']['start'] + $names['names']['offset'];
  
  $name = null;
  foreach ($names['details'] as $key => $platform) {
    if (isset($name))
      break;
    // Check all the names.    
    
    // Font
    if ((isset($platform[6]))) $name = $platform[6];
    // Full Name
    if ((!isset($name)) && (isset($platform[4]))) $name = $platform[4];
    // Family Name
    if ((!isset($name)) && isset($platform[1])) $name = $platform[1];
    
    if (isset($name)) {
      $unicode = (($key == 3) || ($key == 0));
    }
  }
  
  if (isset($name)) {
    $d_offset = $offset + $name['offset'];
    $d_length = $name['length'];
    $s = _magick_ttf_retrieve_string($f, $d_offset, $d_length, $unicode);
  } else 
    $s = false;
  
  _magick_ttf_closefile($f);
  
  return $s;
}

