<?php

function magick_preview($filename) {

  require_once 'magick_const.inc'; // load $preview_caption and $preview_filter

  $basename = basename($filename);
   
  $dest_relative = file_directory_path().'/'._magick_path().'/fonts/'.$basename.'.gif'; // for img tag
  $dest_absolute = getcwd().'/'.$dest_relative; // for convert
  
  if (!file_exists($dest_relative)) {  
  
    $src = 'caption:'.$preview_caption;    
    $font_filepath = getcwd().'/'.file_directory_path().'/'._magick_path().'/fonts/'.$filename;
    $filter = $preview_filter.'  -font "'.$font_filepath.'"';

    // run converter
    _magick_convert($filter,$src,$dest_absolute);
  }  

  $theme = theme('magick_image', $dest_relative, $preview_caption, $preview_caption, null); 
  return $theme;
}

function theme_magick_image($path, $alt='', $title='', $attributes=array()) {
  $attributes = drupal_attributes($attributes);
  if (!is_file($path)) {
    return $title;
  }
  $url = (url($path) == $path) ? $path : (base_path() . $path);
  return '<img src="'. check_url($url) .'" alt="'. check_plain($alt) .'" title="'. check_plain($title) .'" '. $image_attributes . $attributes .' />';
}