<?php

// Image Magick parameters to choose from
$allowed_commands = array('background','fill','stroke','strokewidth','pointsize','gravity','size','font','bgimage','quality','colors','shade','blur', 'transparent');

// Text for preview of font
$preview_caption = 'ABCDEF1234567890��';

// Formating of preview of font
$preview_filter = '-size 200x30  -pointsize 14  -background white  -fill black';
