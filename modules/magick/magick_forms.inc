<?php

/*
  These process the incoming fonts, to see if they are allowable
  Imagemagick fonts.
*/

function  magick_font_upload($form_values=null) {
  // Provide only an upload form
  // With preview of the font
  // And the necessity to provide it a name
  if (!$form_values) {
    $form_values['step'] = true;
  }
  
  drupal_set_message( 'upload' );
  // Simple multi-step form, which can repeat preview if necessary
  if ($_POST['step'] == $form_values['step']) {
    _magick_font_process($form_values);
    if (isset($form_values['filename']) && ($form_values['filename'])) {
      $image = magick_preview($form_values['filename']);
      $form['preview-font'] = array('#value' => theme('magick_font_preview', $form_values['name'], $image ) );
    }

    // now do form errors if necessary
    if ($form_values) {
      if (!$form_values['name']) {
        drupal_set_message( t('Please enter name for font.'), 'error' );
      }
      if (!$form_values['filename']) {
        drupal_set_message( t('Please upload font file.'), 'error' );
      }
      // Successful submit?
      if ($form_values['op'] == t('Add Font')) {
        if ($form_values['name'] && $form_values['filename']) {
          // ok, so we have everything we need
          $name = $form_values['name'];
          $filename = $form_values['filename'];
          $s = getcwd() . '/'. file_directory_path() . '/' . _magick_path() . '/fonts/' . $filename;
          
          drupal_set_message( 'Checking existance of ' . $s );
          if (is_file($s)) {
            drupal_set_message( 'Inputting into database.' );
            db_query("INSERT INTO {magick_fonts} (name, filename) VALUES ('%s', '%s')", $name, $filename);
            drupal_goto('admin/settings/magick/fonts');
          } 
        }
      }
    }
  }
  

  $form['#attributes'] = array("enctype" => "multipart/form-data");
  
  $form['name'] = array(
    '#type' => 'textfield', 
    '#title' => t('Font Name'), 
    '#size' => 60, 
    '#maxlength' => 128, 
    '#description' => t('Name of Font.'), 
    '#default_value' => $form_values['name'],
  );
  
  $form['font'] = array( 
    '#type' => 'file', '#title' => t('Font'), '#size' => 80, 
    '#description' => t('Click "Browse..." to select a font to upload.'), 
  );
  
  $form['preview'] = array( '#type' => 'button', '#value' => t('Preview') );
  $form['save'] = array( '#type' => 'submit', '#value' => t('Add Font'), '#enabled' => ($form_values['name'] && $form_values['filename']) );

  // necessary to get processing of the form correct  
  $form_values['step'] = !$form_values['step'];
  $form['step'] = array( '#type' => 'hidden', '#value'=> $form_values['step'] );
  $form['filename'] = array( '#type' => 'hidden', '#value' => $form_values['filename'] );
  
  $form['#multistep'] = true;
  $form['#redirect'] = false;
  
  return $form;
}

function  magick_font_upload_submit($form_id, $form_values) {
  // If everything is OK, then insert font into database
  // It is possible to have multi fonts of exactly the
  // same style, but from our perspective, this is the fault
  // of the admin. 
  drupal_set_message( 'submit ' );
}

function  magick_fonts($form_values=null) {
  // List the entire set of fonts which exist
  // The preview for the font is the base font filename, plus .preview.gif
  // Allow both addition and deletion of fonts
  if (isset($form_values['op']) && ($form_values['op'] == t('Delete Fonts'))) {
    $form['fonts'] = array(
      '#type' => 'fieldset',
      '#title' => t('Fonts to be deleted'), 
      '#collapsible' => FALSE, 
      '#collapsed' => FALSE,
      '#tree' => TRUE
    );
    
    if (isset($form_values['fonts'])) {
      foreach ($form_values['fonts'] as $key => $value) {
        if (is_numeric($key)) {
          if ($value['check']) {
            // Will have to load the database details for this UGGHHH.
            $form['fonts'][$key]['fid'] = array( '#type' => 'hidden', '#value' => $value['fid'] );
            $form['fonts'][$key]['preview'] = array( '#value' => $value['hpreview'] );
            $form['fonts'][$key]['name'] = array( '#value' => $value['hname'] );      
            $form['fonts'][$key]['confirm'] = array( '#value' => t('is to be deleted.') );
          }
        }
      }

      $form['delete'] = array( '#type' => 'submit', '#value' => t('Confirm Delete') );
    } else {
      $form['fonts']['invalid'] = array( '#prefix' => '<div>', '#value' => t('No fonts selected.'), '#suffix' => '</div>' );
    }
    $form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel') );
  } else {
    $form['fonts'] = array(
      '#type' => 'fieldset',
      '#title' => t('Fonts'), 
      '#collapsible' => TRUE, 
      '#collapsed' => FALSE,
      '#tree' => TRUE
    );
    
    $results = db_query( "SELECT * FROM {magick_fonts} ORDER BY name ASC", $nid );
    while ($obj = db_fetch_object($results)) {
      if (isset($obj->fid)) {
        $form['fonts'][$obj->fid]['fid'] = array( '#type' => 'hidden', '#value' => $obj->fid );
        $form['fonts'][$obj->fid]['hname'] = array( '#type' => 'hidden', '#value' => $obj->name );
        $form['fonts'][$obj->fid]['hpreview'] = array( '#type' => 'hidden', '#value' => magick_preview($obj->filename) );
        $form['fonts'][$obj->fid]['check'] = array( '#type' => 'checkbox' );
        $form['fonts'][$obj->fid]['preview'] = array( '#value' => magick_preview($obj->filename) );
        $form['fonts'][$obj->fid]['name'] = array( '#value' => $obj->name );      
      }
    }
  
    $form['add'] = array( '#type' => 'submit', '#value' => t('Add Fonts') );
    $form['delete'] = array( '#type' => 'submit', '#value' => t('Delete Fonts'), '#description' => t('Please note that deletion will assume confirmation.') );
  }

  $form['#multistep'] = true;
  $form['#redirect'] = false;

  return $form;
}

function  magick_fonts_submit($form_id, $form_values) {
  // Now process the form
  $op = $form_values['op'];
  switch ($op) {
    case t('Add Fonts'):
      drupal_goto('admin/settings/magick/fonts/add');
      break;
    case t('Delete Fonts'):
      // this should get handled in the reprocessing of the form
      break;
    case t('Confirm Delete'):
        // construct listing to delete
        $deletion = array();
        foreach ($form_values['fonts'] as $key => $value) {
          if (is_numeric($key)) {
            $deletion[] = $key;
          }
        }
        
        if (count($deletion) > 0) {
          // No going back now
          drupal_set_message( t('Deleted fonts') . ':' );
          // load fonts details
          $results = db_query("SELECT * FROM {magick_fonts} WHERE fid IN (" . implode(',', $deletion ) . ")");
          while ($obj = db_fetch_object($results)) {
            // print name before deleting the files associated with it.
            drupal_set_message( $obj->name );
            $s = file_directory_path() .'/'. _magick_path() . '/fonts/' . $obj->filename;
            file_delete( $s );
            file_delete( $s . '.gif' );
          }
          db_query("DELETE FROM {magick_fonts} WHERE fid IN (" . implode(',', $deletion) . ")");
        }
        drupal_goto('admin/settings/magick/fonts');
      break;
    case t('Cancel'):
      // force cancelling of the details
        drupal_goto('admin/settings/magick/fonts');
      break;
  }      
}

function _magick_font_process(&$form_values) {
  // Handle any file uploads
  drupal_set_message( 'process' );
  if ($file = file_check_upload('font')) {
    require_once 'magick_ttf.inc';
    $name = _magick_ttf_locate_name($file->filepath);
    drupal_set_message( $form_values['step'] . ' === ' . $name );
    if ($name === false) {
      form_set_error( 'font', t('Invalid font file.') );
      file_delete($file->filepath);
    } else {
      $filename = $file->filename;
      $file = file_save_upload('font', _magick_path() . '/fonts/'. $filename);
      if ($file) {
        drupal_set_message( 'About to change filenames ' . $form_values['filename'] . ' => ' . basename($file->filepath) );
        
        // delete any existing filename currently there.
        if ($form_values['filename']) {
          $s = file_directory_path(). '/' . _magick_path() . '/fonts/' . $form_values['filename'];
          $sp = file_directory_path(). '/' . _magick_path() . '/fonts/' . $form_values['filename'] . '.gif' ;
          drupal_set_message( 'Getting rid of the old magick files ' . $s . $sp );
          file_delete( $s );
          file_delete( $sp );
        }
        // Force a change in the name
        $form_values['name'] = $name;
        $_POST['name'] = $name;
        $form_values['filename'] = basename($file->filepath);
      }
    }
  }
  drupal_set_message( print_r($form_values) );
}

function theme_magick_font_preview($name, $image) {
  drupal_set_message( $image );
  $output = '<div class="preview">';
  $output .= '<h3>'. $name .'</h3>';
  $output .= $image;
  $output .= "</div>";
  return $output;
}