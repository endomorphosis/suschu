<?php
// $Id: zoomify.inc,v 1.2.2.2 2008/01/25 21:41:35 kratib Exp $
/**
 * @file Utility and API functions for zoomify.module
 */

function _zoomify_images($node) {
  $images = array();
  foreach (module_implements('images') as $module) {
    $these_images = module_invoke($module, 'images', $node);
    if (!empty($these_images)) {
      $images = $images + $these_images;
    }
  }
  return $images;
}

function _zoomify_basepath() {
  $basepath = file_directory_path().'/zoomify';
  if (!file_check_directory($basepath, FILE_CREATE_DIRECTORY)) {
    $msg = t('Could not create path %path', array('%path' => $basepath));
    drupal_set_message($msg, 'error');
    watchdog('zoomify', $msg, WATCHDOG_ERROR);
  }
  return $basepath;
}

function _zoomify_nodepath($node) {
  return file_create_path(_zoomify_basepath().'/'.$node->nid);
}

function _zoomify_filepath($node, $fid) {
  return file_create_path(_zoomify_nodepath($node).'/'.$fid);
}

function _zoomify_identical($node, $fid, $filepath) {
  $md5 = md5_file($filepath);
  $md5_path = _zoomify_filepath($node, $fid).'/Image.md5';
  if (!file_exists($md5_path)) return FALSE;
  $md5_existing = file_get_contents($md5_path);
  return $md5_existing == $md5;
}

function _zoomify_insert_node($node) {
  $images = _zoomify_images($node);
  if (empty($images)) return;
  foreach ($images as $fid => $filepath) {
    _zoomify_process($node, $fid, $filepath);
  }
}

function _zoomify_process($node, $fid, $filepath) {
  // TODO: Let user adjust maximum time for scripts or provide better mechanism.
  set_time_limit(0);
  $info = image_get_info($filepath);
  if ($info === FALSE) {
    $msg = t('Could not find image info for file %path.', array('%path' => $filepath));
    watchdog('zoomify', $msg, WATCHDOG_ERROR);
    drupal_set_message($msg, 'error');
    return;
  }
  if (($info['width'] < variable_get('zoomify_minimum_width', 1024))
   && ($info['height'] < variable_get('zoomify_minimum_height', 768))) return;

  // Call the Python tilemaker script on the input file.
  $pathinfo = pathinfo_filename($filepath);
  $cmd = 'python '.drupal_get_path('module', 'zoomify').'/ZoomifyImage/ZoomifyFileProcessor.py'.
         ' "'.$filepath.'"'.
         ' 2>&1';
  $return = 0;
  $output = array();
  exec($cmd, $output, $return);
  if ($return) {
    $msg = t('ZoomifyFileProcessor.py returned an error:<br />!output', array('!output' => implode('<br />', $output)));
    watchdog('zoomify', $msg, WATCHDOG_ERROR);
    drupal_set_message($msg, 'error');
    return;
  } 
  else {
    $msg = t('Tiles created for file %path.', array('%path' => $filepath));
    watchdog('zoomify', $msg, WATCHDOG_NOTICE);
    drupal_set_message($msg, 'status');
  }

  if (!file_check_directory(_zoomify_nodepath($node), FILE_CREATE_DIRECTORY)) {
    $msg = t('Could not create path %path.', array('%path' => _zoomify_nodepath($node)));
    drupal_set_message($msg, 'error');
    watchdog('zoomify', $msg, WATCHDOG_ERROR);
  }
  rename($pathinfo['dirname'].'/'.$pathinfo['filename'], _zoomify_filepath($node, $fid));
  $md5 = md5_file($filepath);
  $md5_path = _zoomify_filepath($node, $fid).'/Image.md5';
  file_put_contents($md5_path, $md5);
}

function _zoomify_delete_node($node) {
  if (is_dir(_zoomify_nodepath($node))) {
    rrmdir(_zoomify_nodepath($node));
    $msg = t('Tiles deleted for node %nid.', array('%nid' => $node->nid));
    watchdog('zoomify', $msg, WATCHDOG_NOTICE);
    drupal_set_message($msg, 'status');
  }
}

function _zoomify_update_node($node) {
  $old_images = file_scan_directory(_zoomify_nodepath($node), '.*', array('.', '..'), 0, FALSE);
  $images = _zoomify_images($node);
  foreach ($images as $fid => $filepath) {
    if (!_zoomify_identical($node, $fid, $filepath)) {
      rrmdir(_zoomify_filepath($node, $fid));
      _zoomify_process($node, $fid, $filepath);
    }
  }
  foreach ($old_images as $old_image) {
    if (!array_key_exists($old_image->basename, $images) && is_dir($old_image->filename)) {
      rrmdir($old_image->filename);
      $msg = t('Tiles deleted for obsolete file %fid.', array('%fid' => $old_image->basename));
      watchdog('zoomify', $msg, WATCHDOG_NOTICE);
      drupal_set_message($msg, 'status');
    }
  }
}

