********************************************************************
D R U P A L M O D U L E
********************************************************************
Name:
Zoomify
Author: Karim Ratib <kratib at open dash craft dot com>
Drupal: 5
********************************************************************
DESCRIPTION:

This module integrates the Zoomify component (http://www.zoomify.com)
into Drupal. Zoomify is a Flash application that displays large 
images by allowing to pan and zoom into them. Images must be first
preprocessed in order to Zoomify to work, and that's what the Zoomify
Image (aka Agile Image) Python script does 
(http://sourceforge.net/projects/zoomifyimage/).

The integration into Drupal is done by detecting images within nodes. 
When the module detects that a new image has a resolution larger than 
some preset size, it invokes the Python script that cuts up the image 
into tiles. It also adds a link to the node that displays a new page 
containing the Zoomify Flash viewer.

The following image types are currently handled:
* Image nodes
* CCK nodes with image fields (including imagecache effects and views)

********************************************************************
INSTALLATION:

* Install Python 2.5+ and make sure it's in the path.
* Install Python Imaging Library (PIL).
* Download the latest Zoomify package from its site and unpack it in the 
zoomify.module folder such that the root of the package is called "ZoomifyImage".

********************************************************************

