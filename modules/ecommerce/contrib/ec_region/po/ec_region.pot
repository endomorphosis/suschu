# $Id: ec_region.pot,v 1.1.2.1 2007/05/03 15:12:23 darrenoh Exp $
#
# LANGUAGE translation of Drupal (ec_region.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# No version information was available in the source files.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 11:00-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ec_region.module:18;25
msgid "Regions"
msgstr ""

#: ec_region.module:19
msgid "Define geographic regions that are used by other modules."
msgstr ""

#: ec_region.module:31
msgid "Delete region"
msgstr ""

#: ec_region.module:37;104
msgid "Edit"
msgstr ""

#: ec_region.module:44
msgid "Add"
msgstr ""

#: ec_region.module:52
msgid "Realm: "
msgstr ""

#: ec_region.module:65
msgid "Default"
msgstr ""

#: ec_region.module:90
msgid "<p>This is your list of regions. The \"Realm\" refers to a specific area of E-Commerce\n             For example, you could have a \"Local\" region\n             for Tax and Shipping purposes, with different geographies assigned to each.</p>"
msgstr ""

#: ec_region.module:94 ec_region.info:0
msgid "Region"
msgstr ""

#: ec_region.module:94
msgid "Operations"
msgstr ""

#: ec_region.module:105;192
msgid "Delete"
msgstr ""

#: ec_region.module:106
msgid "Configure"
msgstr ""

#: ec_region.module:112;410
msgid "Add region"
msgstr ""

#: ec_region.module:141
msgid "Region name"
msgstr ""

#: ec_region.module:144
msgid "The name of this region. Examples: \"Interstate\", \"Taxable destinations\""
msgstr ""

#: ec_region.module:148
msgid "Region used by"
msgstr ""

#: ec_region.module:151
msgid "The area of E-Commerce where this region will be used."
msgstr ""

#: ec_region.module:155
msgid "Submit"
msgstr ""

#: ec_region.module:165
msgid "The region %name has been updated."
msgstr ""

#: ec_region.module:188
msgid "Are you sure you want to delete the region %name?"
msgstr ""

#: ec_region.module:191
msgid "This action can not be undone, you will lose any country/state associations belonging to his region."
msgstr ""

#: ec_region.module:192
msgid "Cancel"
msgstr ""

#: ec_region.module:201
msgid "The region has been deleted."
msgstr ""

#: ec_region.module:285
msgid "Countries"
msgstr ""

#: ec_region.module:292
msgid "States"
msgstr ""

#: ec_region.module:393
msgid "Update regions"
msgstr ""

#: ec_region.module:406
msgid "<p>Here you can allocate Countries and States to different regions.\n                   Simply select the locations that you need to change and then\n                   click the \"Update regions\" button. Go to the !add_region page\n                   if the only region you have is N/A or you need more regions. </p>"
msgstr ""

#: ec_region.module:470
msgid "The regions have been updated."
msgstr ""

#: ec_region.info:0
msgid "Create regions that can be used by other modules. Optionally use location.module."
msgstr ""

#: ec_region.info:0
msgid "E-Commerce Uncategorized"
msgstr ""

