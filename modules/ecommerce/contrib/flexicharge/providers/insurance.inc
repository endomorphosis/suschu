<?php
// $Id: insurance.inc,v 1.1.2.2.4.2 2007/07/12 02:40:25 gordon Exp $

/**
 * @file
 * insurance.inc
 *
 * This is a module that provides a charge to flexicharge module.
 *
 * It is called "insurance" because charges are applied to everyone
 * at the end of the cart.
 *
 */


function insurance_charge_methods() {
  $methods = array();

  $methods['insurance'] = array(
    '#description' => t('Insurance and Tracking Charges.')
  );
  $methods['insurance']['general'] = array(
    '#title' => t('Insurance and Tracking Charges.'),
  );

  return $methods;
}

/**
 * _flexicharge_form_elements hook.
 *
 * This function is called so that the charge provider can add
 * required fields to the new charge form.
 *
 */
function insurance_flexicharge_form_elements(&$form, $op, $method) {

  switch ($method) {
    case 'general':
      /*
       * Here is an example form element to add.
       *
      $form['elements']['my_settings']['something'] = array(
        '#type' => 'textfield',
        '#required' => true,
        '#title' => t('some description'),
        '#description' => t('This is for...'),
        '#size' => 8,
      );
      // Kill fields
      unset($form['module']['operator']);
      */
      break;
  }

}

/**
 * implementation of hook_flexicharge_form_validate.
 *
 * This function is called so that the charge provider can
 * validate the fields of a new charge on submission. The charge's
 * own form elements can be found at $form['module']['elements'].
 *
 */
function insurance_flexicharge_form_validate($form_id, $form) {

  switch ($method) {
    case 'general':
  }

  return $form;
}

/**
 * Implementation of hook_flexicharge_calculate.
 *
 * We are just catching the 'review' operation of the checkoutapi.
 * This is where most misc calculations will want to work. On the
 * review screen.
 *
 */
function insurance_flexicharge_review(&$txn, $charge) {

  switch ($charge->method) {
    case 'general':
         $misc = array(
           'already_added'    => $charge->already_added,
           'callback'         => 'general_insurance_flexicharge_calculate',
           'chid'             => $charge->chid,
           'description'      => $charge->display,
           'hide_if_zero'     => $charge->hide_if_zero,
           'method'           => $charge->method,
           'operator'         => $charge->operator,
           'rate'             => $charge->rate,
           'subtotal_after'   => $charge->subtotal_before,
           'subtotal_before'  => $charge->subtotal_before,
           'type'             => 'fl_'. $charge->chid,
           'weight'           => $charge->position
         );
      break;
  }
  return $misc;
}

/**
 * 'General' charge calculation
 *
 * FLEXICHARGE_CHARGE                 +/-
 * FLEXICHARGE_CHARGE_PCT_SUBTOTAL    % of current total
 * FLEXICHARGE_CHARGE_PCT_ITEMTOTAL   % of item total
 *
 */
function general_insurance_flexicharge_calculate(&$txn, $misc, $total) {

  $rate = 0;
  // Do the pct if needed
  if (($misc->operator != FLEXICHARGE_CHARGE) && $misc->rate) {
    $factor = 1;
    if($misc->rate < 0 ) {$factor = -1;}
    $pct = ($misc->rate * $factor) / 100;
  }

  switch ($misc->operator) {
    case FLEXICHARGE_CHARGE:
      $rate = $misc->rate;
    case FLEXICHARGE_CHARGE_PCT_SUBTOTAL:
      $rate = ($total * $pct * $factor);
    case FLEXICHARGE_CHARGE_PCT_ITEMTOTAL:
      $value = 0;
      foreach ((array)$txn->items as $item) {
        if (product_has_quantity($item)) {
          $value += ($item->price * $item->qty);
        }
        else {
          $value += $item->price;
        }
      }
      $rate = ($value * $pct * $factor + 2.70);
   }
  // hide this charge if required.
  if (empty($rate) and !empty($misc->hide_if_zero)) {
    foreach ($txn->misc as $m) {
      if ($m->chid == $misc->chid) {
        $txn->misc[$m->chid]->seen = true;
      }
    }
  }
  return $rate;
}


/**
 * Implementation of hook_flexicharge_checkoutapi.
 *
 * Perhaps most methods won't catch this, because we break have broken out the review
 * $op into hook_flexicharge_calculate
 *
 */
function insurance_flexicharge_checkoutapi(&$txn, $op, $arg3 = NULL, $arg4 = NULL) {
  switch ($op) {
    case 'form':
      break;
    case 'validate':
      break;
    case 'save':
      break;
  }
}