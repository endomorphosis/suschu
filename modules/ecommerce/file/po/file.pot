# $Id: file.pot,v 1.1.2.1 2007/05/03 14:52:05 darrenoh Exp $
#
# LANGUAGE translation of Drupal (file.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  file.module,v 1.37.2.7.2.1.2.13 2007/03/20 05:00:41 neclimdul
#  file.install,v 1.3.6.2 2007/02/28 08:08:37 neclimdul
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2007-05-03 10:36-0400\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: file.module:16
msgid "A file can be downloaded by purchasers."
msgstr ""

#: file.module:18
msgid "Some files may not be available until your transactions has finished being processed."
msgstr ""

#: file.module:35
msgid "File autocomplete"
msgstr ""

#: file.module:42
msgid "My files"
msgstr ""

#: file.module:50
msgid "Download"
msgstr ""

#: file.module:58
msgid "Product file quicklist"
msgstr ""

#: file.module:65
msgid "File"
msgstr ""

#: file.module:78
msgid "User Files"
msgstr ""

#: file.module:133
msgid "Please enter the path to the file."
msgstr ""

#: file.module:138
msgid "The file doesn't exist."
msgstr ""

#: file.module:152
msgid "File download product"
msgstr ""

#: file.module:164
msgid "Do not edit.  File is managed by %nodetype node."
msgstr ""

#: file.module:168
msgid "Enter the filesystem path to this file (not the URL). This path will be prefixed with <b>!file_path</b> Here is a <a href=\"!file_quicklist\" onclick=\"window.open(this.href, '!file_quicklist', 'width=480,height=480,scrollbars=yes,status=yes'); return false\">list of files</a> in this directory. You may need to FTP your file to this directory before you can create the file product."
msgstr ""

#: file.module:174
msgid "File settings"
msgstr ""

#: file.module:178
msgid "File path"
msgstr ""

#: file.module:236
msgid "There are shippable items in your cart that may cause your files to not be immediately available."
msgstr ""

#: file.module:245
msgid "Ecommerce file download settings (%revision)"
msgstr ""

#: file.module:253
msgid "File path for products"
msgstr ""

#: file.module:257
msgid "A file system path where the product files are stored. This directory has to exist and be writable by Drupal. This directory should not be accessible over the web. Changing this location after the site has been in use will cause problems so only change this setting on an existing site if you know what you are doing."
msgstr ""

#: file.module:264
msgid "Default renewal schedule"
msgstr ""

#: file.module:266
msgid "Select a default renewal schedule to be used for files. Note: This is not used yet."
msgstr ""

#: file.module:289
msgid "Downloads"
msgstr ""

#: file.module:290
msgid "View your downloads"
msgstr ""

#: file.module:292
msgid "History"
msgstr ""

#: file.module:311
msgid "name"
msgstr ""

#: file.module:311;359
msgid "size"
msgstr ""

#: file.module:311
msgid "changed"
msgstr ""

#: file.module:346
msgid "<p><a href=\"!files-uri\">Click here to view your files.</a></p>"
msgstr ""

#: file.module:347
msgid "%username's expired files"
msgstr ""

#: file.module:351
msgid "<p><a href=\"!expired-files-uri\">Click here to view your expired files.</a></p>"
msgstr ""

#: file.module:352
msgid "%username's files"
msgstr ""

#: file.module:358
msgid "filename"
msgstr ""

#: file.module:360
msgid "expired"
msgstr ""

#: file.module:360
msgid "expires"
msgstr ""

#: file.module:361
msgid "operations"
msgstr ""

#: file.module:377
msgid "<a href=\"!file-download-uri\">download</a>"
msgstr ""

#: file.module:380
msgid "Transaction %status"
msgstr ""

#: file.module:384
msgid "%interval ago"
msgstr ""

#: file.module:394
msgid "You have no files to download."
msgstr ""

#: file.module:92
msgid "create file products"
msgstr ""

#: file.module:92
msgid "edit own file products"
msgstr ""

#: file.module:0
msgid "file"
msgstr ""

#: file.install:27
msgid "E-Commerce: File tables have been created."
msgstr ""

#: file.info:0
msgid "File Product"
msgstr ""

#: file.info:0
msgid "Creates a file product."
msgstr ""

#: file.info:0
msgid "E-Commerce Product Types"
msgstr ""

