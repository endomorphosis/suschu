// $Id: address.js,v 1.1.2.3.4.1 2007/02/03 11:04:45 gordon Exp $

if (Drupal.jsEnabled) {
  $(document).ready( function () {
    $('#edit-country').each( function () {
      Drupal.addressToggleFields(this.options[this.selectedIndex].value);
    });
    $('#edit-state').each( function () {
      this.remove(this.length-1);
    });
    $('#edit-country').change( function() {
      Drupal.addressToggleFields(this.options[this.selectedIndex].value);
    });
  });
}

Drupal.addressToggleFields = function(country) {
  if (country == 'us') {
    $('#wrapper-province').hide();
    $('#wrapper-state').show();
  }
  else {
    $('#wrapper-province').show();
    $('#wrapper-state').hide();
  }
}
